#define _XTAL_FREQ 32000000

#define Trigger PORTBbits.RB6

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include <plib.h>
#include "mcu_config.h"
#include "uart.h"

void interrupt ISR()
{
    if(PIR1bits.TX1IF)
    {
        PIR1bits.TX1IF = 0;
    }
}

volatile char DMX_Def[17] = {128,128,255,255,255,0,255,0,0,0,0,255,0,0,0,0,255};

volatile unsigned int i = 0,
                      j = 0,
                      TriggerDebounce = 200;

int main()
{
    OSCCONbits.IRCF = 0b110;
    TRISBbits.TRISB6 = 1;
    UART_Init();
    while(1)
    {
        
        if(TriggerDebounce < 200)
        {
            TriggerDebounce++;
        }
        if(!Trigger && TriggerDebounce == 200)
        {
            TriggerDebounce = 0;
            
            if(DMX_Def[16] == 255)
            {
                DMX_Def[16] = 0;
            }
            else
            {
                DMX_Def[16] = 255;
            }
        }
        DMX_Break();
        CLRWDT();                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
        while(UART_TXBufferFull());
        UART_Write(0);
        for(i = 0; i <= 16; i++)
        {
            while(UART_TXBufferFull());
            UART_Write(DMX_Def[i]);
            CLRWDT();
        }
        for(i = 0; i <= 492; i++)
        {
            while(UART_TXBufferFull());
            UART_Write(0);
            CLRWDT();
        }
        
    }
    
}


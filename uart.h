#define UART_TXBufferFull() (!TXSTAbits.TRMT)

void UART_Init()
{
    TXSTAbits.TX9 = 1; //enable 9 bit transmit
    TXSTAbits.BRGH = 1; //high speed mode
    TXSTAbits.SYNC = 0; //asynchronous mode
    RCSTAbits.SPEN = 1; //serial port enabled
    TXSTAbits.TXEN = 1; //transmit enabled
    
    //TRISBbits.TRISB7 = 1;
    //TRISBbits.TRISB5 = 1;
    
    SPBRG = 7; //250kBaud
    
    
}

void UART_Write(char data)
{
    TXREG = data;
}

void DMX_Break()
{
    RCSTAbits.SPEN = 0;
    TXSTAbits.TXEN = 0;

    TRISBbits.TRISB7 = 0;
    LATBbits.LATB7 = 0;
    __delay_us(100);
    __delay_us(100);
    __delay_us(100);
    LATBbits.LATB7 = 1;
    __delay_us(30);
    TRISBbits.TRISB7 = 1;
    RCSTAbits.SPEN = 1;
    TXSTAbits.TXEN = 1;
    
    
}